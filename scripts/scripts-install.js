if (!require('fs').existsSync('pe-core')) {
  const packageJson = require(`${__dirname}/../package.json`);
  const version = packageJson['protopia-ecosystem'].core;
  require('child_process').execSync(
    `git clone https://gitlab.com/dilesoft-projects/pe-node-core --branch ${version} pe-core`,
    { stdio: 'inherit' },
  );
}
