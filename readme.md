#Сервер системы управления Дома Протопии#

## Установка на чистую систему ##

1. Установите базу данных mongodb
1. `npm install`
1. `npm run pe-config`
1. `npm run pe-install`
1. `npm start`

## Установка на систему с существующей БД ##

1. Скопируйте в папку config db_config.json и server_config.json
1. `npm install`
1. `npm run pe-update`
1. `npm start`

Доступ к консоли для тестирования команд: http://localhost:9095/graphql
